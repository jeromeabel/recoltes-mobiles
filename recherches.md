# Axes de recherches

Je vois plusieurs axes de recherches possibles pour la création d'une oeuvre artistique certainement visible sous la forme d'une installation :
- les relations d'échelles entre cet objet électronique miniature, embarqué donc proche de l'homme et celui de la ville, de son urbanisme, de ses passages et territoires, de ses dessins parfois éloignés de l'homme. Il y a potentiellement un aspect intéressant à travailler ici : la relation au déplacement (prothèse, guide, dérive, ...) et son archivage numérique souvent sans notre accord ; 
- la dimension matérielle et des externalités écologiques de ce type d'appareil;
- le besoin de communiquer (+ quantité : nombres d'appareils, nombre de connexions, nombre de messages)  et le rapport avec notre intériorité (outillage de notre personnalité, silence, attention);
- le teléphone portable, objet de convoitise, vols, injonction, pressions;
- le téléphone portable, comme donneur d'ordre des micro-tâches, comme outil de déconnection de la réalisation concrètes/humaines des services;
- intérieur / extérieur : forme en creux de téléphone (moule) avec des matières utilisées dans l'espace public pratiques à utiliser pour créer des formes : béton = voxel, plâtre, gel transparent pour électronique, ...


## Axe 1 : matières
Mots clés : dechets, géopolitique, trou (cradle), terre, processus/temps, penser la limite, accaparement rendue possible par l'idée générale de Nature.


<img src="dessins/carottes.jpg" width="700px">

Des échantillons géologiques de terrains/sols sont sorties, sous forme de forage de carottes et placés comme un monument en ruine. Des colonnes qui jadis étaient le pilier de la terre.

Voir référence aux carottes glaciaires, archives climatiques 2800m pour -1 000 000 ans ou -800 000 ans :
- [Que peut nous apprendre l’histoire du climat inscrite dans les glaces de l’Antarctique ?](https://www.franceinter.fr/emissions/l-edito-carre/l-edito-carre-13-janvier-2020)

<img src="dessins/carottes-details.jpg" width="350px">

Ces carottes sont composés de trois parties : l'échantillon géologique, au milieu un composant électronique de téléphone, plus bas le même volume en béton.

Plusieurs idées traversent ces dessins, nous pouvons en parler.

----

<img src="dessins/carotte-beton.jpg" width="350px">

Carottes de béton. Jeux vide/plein, intérieur(miniaturisations)/extérieur(communications) avec le moule d'une coque de téléphone portable. 

----

<img src="dessins/feu.jpg" width="700px">

Autre idée : une boule transparente contient une bûche en feu et sa fumée. Un dispositif de captation permettant à plusieurs téléphones de prendre la même situation sous différents angles permettrait une sorte de vidéo en live de la dégradation de cette bûche, comme nous regardons la terre se dégrader.

----

<img src="dessins/sable.jpg" width="700px">

Autre idée : une boule transparente contient du sable. Celui se déverse lentement sur des reproductions de téléphones (moulage) pour signifier le passage du temps dans les technologies et leur surproduction.

## Axe 2 : messages
Mots clés : informations, messages, plus jamais seul. Surface-clic. Peur du silence. Sollicitations vs Introspection. Pression économique/médiatique. Apprendre par coeur VS google. 


<img src="dessins/messages.jpg" width="700px">

Un câble tendu entre deux édifices ou arbres pourrait permettre d'envoyer des messages dans la nuit en utilisant la rémanence de l'oeil ou du temps de pose photographique (~ light painting). L'idée à développer est l'envoi de messages électroniques par le biais de mécanismes physiques actionnables à la main et en jouant sur la gravité.

----

<img src="dessins/reperes.jpg" width="700px">

Autre idée : une prothèse autour du téléphone comme outil de guide personnalisé. Le téléphone au milieu d'une structure filaire, connecté, entre prisons et structures de lien.

----

<img src="dessins/joyau.jpg" width="700px">

Images de diamants, joyaux, rubis (récompenses de jeux vidéo) sur une multitude de téléphone. Le téléphone comme objet de convoitise au delà de son utilisation.

