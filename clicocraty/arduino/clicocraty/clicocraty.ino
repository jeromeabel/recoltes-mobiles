// Clicocraty : Ferme à clics
// mpr121 > i2c > Arduino > Serial (write)

#include <Wire.h>
#include "Adafruit_MPR121.h"

#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif

#define DEBUG false

Adafruit_MPR121 cap = Adafruit_MPR121();


// Keeps track of the last pins touched
// so we know when buttons are 'released'
uint16_t lasttouched = 0;
uint16_t currtouched = 0;

void setup() {
  Serial.begin(9600);

  while (!Serial) { // needed to keep leonardo/micro from starting too fast!
    delay(10);
  }
  
  // Default address is 0x5A, if tied to 3.3V its 0x5B
  // If tied to SDA its 0x5C and if SCL then 0x5D
  if (!cap.begin(0x5A)) {
    if ( DEBUG ) Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  if ( DEBUG ) Serial.println("MPR121 found!");
}

void loop() {
  // Get the currently touched pads
  currtouched = cap.touched();
  
  for (uint8_t i = 0; i < 12; i++) {
    // it if *is* touched and *wasnt* touched before, alert!
    if ((currtouched & _BV(i)) && !(lasttouched & _BV(i)) ) {
      if ( DEBUG ) {
		    Serial.print(i); 
		    Serial.println(" touched");
	    } else {
	      Serial.write(i);
	    }
    }

    // if it *was* touched and now *isnt*, alert!
    if (!(currtouched & _BV(i)) && (lasttouched & _BV(i)) ) {
      if ( DEBUG ) {
		    Serial.print(i); 
		    Serial.println(" released");
		  }
    }
  }

  // reset our state
  lasttouched = currtouched;

  delay(10);
}
