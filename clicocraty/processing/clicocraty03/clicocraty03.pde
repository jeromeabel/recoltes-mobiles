// Clicocraty
// Capacitive sensors > Serial number (0-12) > 

import processing.serial.*;

boolean SERIAL = true;
boolean DEBUG = true;
int HITS = 10;
int NB_SVG = 8; // max 8
int SVG_WIDTH = 400;


Serial myPort;
PShape[] svgUp = new PShape[NB_SVG];
PShape[] svgDown = new PShape[NB_SVG];
color colorDown = color(255,100,120);
color colorUp = color(100,200,120);
int countUp = 0;
int countDown = 0;
int hitCountUp = HITS;
int hitCountDown = HITS;
int svgUpIndex = 2;
int svgDownIndex = 1;

int sensorValue = 0;
boolean hitUp = false;
boolean hitDown = false;

void setup() 
{
  size(1600, 900);
  
  //printArray(Serial.list());
  //String portName = Serial.list()[0];
  if(SERIAL) myPort = new Serial(this, "/dev/ttyACM0", 9600);
  
  // Load svg
  for (int i = 0; i < NB_SVG; i++)
  {
    svgUp[i] = loadShape("up-" + (i + 1) + ".svg");
    svgUp[i].disableStyle();
    svgDown[i] = loadShape("down-" + (i + 1) + ".svg");
    svgDown[i].disableStyle();
  }
  
  // Styles
  noStroke();
  textSize(64);
}

void draw()
{
  background(255);
  
  if ( SERIAL && myPort.available() > 0 ) {
    sensorValue = myPort.read();
    if (DEBUG) println(sensorValue);
    
    if (sensorValue > 5)  {
      countUp++;
      svgUpIndex = int(random(NB_SVG));
      hitCountUp = HITS;
      hitUp = true;
    }
    else {
      countDown++;
      svgDownIndex = int(random(NB_SVG));
      hitCountDown = HITS;
      hitDown = true;
    }
  }
  
  // Down / Left
  if (hitDown)  {
    fill(242);
    hitCountDown--;
    if (hitCountDown < 0) hitDown = false;
  }
  else fill(255);
  rect(0, 0, width/2, height);
  fill(colorDown);
  shape(svgDown[svgDownIndex], 200, 150, SVG_WIDTH, SVG_WIDTH * svgDown[svgDownIndex].height / svgDown[svgDownIndex].width ); 
  text(countDown, width/4, 750);
  
  // Up / Right
  if (hitUp) {
    fill(242);
    hitCountUp--;
    if (hitCountUp < 0) hitUp = false;
  }
  else fill(255);
  rect(width/2, 0, width/2, height);
  fill(colorUp);
  shape(svgUp[svgUpIndex], width/2 + 200, 150, SVG_WIDTH, SVG_WIDTH * svgUp[svgUpIndex].height / svgUp[svgUpIndex].width ); 
  text(countUp, width*3/4, 750);
}
