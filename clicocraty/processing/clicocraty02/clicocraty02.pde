// Ferme à clics
// Capacitive sensors > Serial number (0-12) > 

import processing.serial.*;

boolean SERIAL = true;
boolean DEBUG = true;
int HITS = 10;

Serial myPort;
PShape up, down;
color colorUp = color(255,100,120);
color colorDown = color(100,200,120);
int countUp = 0;
int countDown = 0;
int hitCountUp = HITS;
int hitCountDown = HITS;

int sensorValue = 0;
boolean hitUp = false;
boolean hitDown = false;

// PImage, loadImage, requestImage

void setup() 
{
  size(800, 600);
  
  //printArray(Serial.list());
  //String portName = Serial.list()[0];
  if(SERIAL) myPort = new Serial(this, "/dev/ttyACM0", 9600);
  
  // Load svg
  up = loadShape("emoji_01.svg");
  down = loadShape("emoji_02.svg");
  
  // Styles
  up.disableStyle();
  down.disableStyle();
  noStroke();
  textSize(32);
}

void draw()
{
  background(255);
  
  if ( SERIAL && myPort.available() > 0 ) {
    sensorValue = myPort.read();
    if (DEBUG) println(sensorValue);
    
    if (sensorValue > 5)  {
      countUp++;
      hitCountUp = HITS;
      hitUp = true;
    }
    else {
      countDown++;
      hitCountDown = HITS;
      hitDown = true;
    }
  }
  
  // Down / Left
  if (hitDown)  {
    fill(242);
    hitCountDown--;
    if (hitCountDown < 0) hitDown = false;
  }
  else fill(255);
  rect(0, 0, width/2, height);
  fill(colorDown);
  shape(down, 100, 150, 200, 200);
  text(countDown, 200, 500);
  
  // Up / Right
  if (hitUp) {
    fill(242);
    hitCountUp--;
    if (hitCountUp < 0) hitUp = false;
  }
  else fill(255);
  rect(width/2, 0, width/2, height);
  fill(colorUp);
  shape(up, width/2 + 100, 200, 200, 200); 
  text(countUp, 600, 500);
}
