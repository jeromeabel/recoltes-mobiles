import processing.serial.*;

Serial myPort;
PImage img;
PShape svg;
int imgCount = 12;
PImage[] imgs = new PImage[imgCount];
PShape[] svgs = new PShape[imgCount];

int val = 0;
int last_val = 0;
int couleur= 255; 

void setup() 
{
  size(800, 600);
  //printArray(Serial.list());
  //String portName = Serial.list()[0];
  // myPort = new Serial(this, "/dev/ttyACM0", 9600);
  
  img = loadImage("pouceHaut.png");
  svg = loadShape("pouces.svg");
  
  // Load images asynchronously
  for (int i = 0; i < imgCount; i++){
    //imgs[i] = requestImage("pouceHaut.png");
    svgs[i] = loadShape("pouces.svg"); 
  }
  
}

void draw()
{
  
  background(couleur);

  /*
  if ( myPort.available() > 0) {
    val = myPort.read();
    println(val);
    couleur = (val + 1) * 50;
  }
  */
  
  //drawImages();
  //image(img, 0, 0);
  //image(img, 0, height/2, img.width/2, img.height/2);
  //shape(svg, 110, 90, 300, 300);  // Draw at coordinate (110, 90) at size 100 x 100
  
  
  drawShapes();
}

void drawImages() {
  int y = (height - imgs[0].height) / 2;
  for (int i = 0; i < imgs.length; i++){
    image(imgs[i], width/imgs.length*i, y, imgs[i].width, imgs[i].height);
  }
}

void drawShapes() {
  //int y = (height - imgs[0].height) / 2;
  int y = 100;
  
  
  for (int i = 0; i < imgs.length; i++){
    svgs[i].disableStyle();  // Ignore the colors in the SVG
  fill(0, random(255), 153);    // Set the SVG fill to blue
  stroke(255);          // Set the SVG fill to white
  
    shape(svgs[i], width/svgs.length*i, y, 100, 100);
  }
}
