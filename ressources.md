# Ressources

## Ressources artistiques

Mes réalisations utilisant le téléphone portable :
- [Infini](http://jeromeabel.net/workshop/infini/)
- [Logariat](http://jeromeabel.net/workshop/logariat/)

Quelques références artistiques intéressantes :

- 1918 : [Mes ballets plastiques](http://www.aparences.net/periodes/art-moderne/le-futurisme/) de Fortunato Depero
- 1927 : [Metropolis](http://fr.wikipedia.org/wiki/Metropolis_(film,_1927)), film de Fritz Lang
- 1935 : les Rotoreliefs de Marcel Duchamp [vidéo](http://vimeo.com/4748112)
- 1952 : Chombart de Lauwe, "Paris et l'agglomération parisienne", CNRS. Trajets pendant un an d'une jeune fille du XVIe arrondissement. 
- 1953 : [Albert Ducrocq](http://fr.wikipedia.org/wiki/Albert_Ducrocq), le renard électrique. [Entretien vidéo](http://www.ina.fr/video/CAF88038611).
- 1955 : Remote control painting d'[Akira Kanayama](http://fr.wikipedia.org/wiki/Kanayama_Akira) qui fait partie du groupe [Gutai](http://fr.wikipedia.org/wiki/Gutai)
- 1956 : Guy Ernest Debord. [Dérive psychogéographique](http://slow.free.fr/clans/pages/litterature/debord_derive.html) et Internationale Situationniste.
- 1960 : Mouvement [Oulipo](https://fr.wikipedia.org/wiki/Oulipo)
- ~1960 : Mouvement [Fluxus](http://www.artwiki.fr/wakka.php?wiki=Fluxus)
- 1963 : Nam June Paik. [Random Access](http://www.nydigitalsalon.org/10/artwork.php?artwork=13)
- 1965 : Nam June Paik. [Magnet TV](http://art-a-lordinateur.blogspot.fr/2012/03/magnet-tv.html) 
- 1967 : Piotr Kowalski. [CUBE NR 10 (CG1967-Q)](http://www.artnet.fr/artistes/piotr-kowalski/cube-nr-10-cg1967-q-X9i4qwoFaXhKfG9RmFZiAw2)
- 1968 : [Whole Earth Catalog](https://fr.wikipedia.org/wiki/Whole_Earth_Catalog)
- 1974 : Theodor H Nelson. Computer Lib.
- 1993 : Joan Heemskerk and Dirk Paesmans, collectif Jodi. [wwwwwwwww.jodi.org](http://wwwwwwwww.jodi.org/)
- 1997 : John F. Simon Jr. [Every Icon](http://www.numeral.com/eicon.html)
- 1998 : Institute For Applied Autonomy. [Grafititwriter](http://www.appliedautonomy.com/gw.html)
- 2001 : Ben Rubin and Mark Hansen. [The Listening Post](http://www.digiart21.org/art/the-listening-post)
- 2001 : Daniel Libeskind. [Musée Juif de Berlin](https://vivreaberlin.com/larchitecture-du-musee-juif-de-berlin.html)
- 2004 : Julius Popp. Bit.Fall
- 2005 : Nobuhiro Nakanishi. [Layer drawings](http://nobuhironakanishi.com/gallery/layer-drawings/)
- 2005 : BenRubin. DarkSource
- 2006 : Ryoji Ikeda. [Datamatics](http://www.ryojiikeda.com/project/datamatics/)
- 2006 : ChrisJordan. RunningTheNumbers
- 2006 : Quayola. ArchitecturalDensity
- 2009 : Free Art and Technology (FAT), OpenFrameworks, the Graffiti Research Lab, and The Ebeling Group communities. [Eyewriter](http://eyewriter.org/) [video](http://vimeo.com/7352063).
- 2009 : Robert Marthy. [Light Frequency Fingertips](http://www.robertmathy.com/lightfrequencyfingertips.html)
- 2009 : V3ga. [Gravity](http://www.v3ga.net/blog2/2009/03/gravity/)
- 2010 : [Dead Drops](http://deaddrops.com)
- 2010 : [What Happened, Happend](http://www.danielpalacios.info/en/whatever_happened) de Daniel Palacios
- 2011 : Senseable City Lab. [Backtalk](http://senseable.mit.edu/backtalk/)
- 2011 : Julian Oliver et Danja Vasiliev. [Newstweek](http://newstweek.com/)
- 2011 : Benjamin Gaulon, Tom Verbruggen, Gijs Gieskes. [Refunct Media](http://www.recyclism.com/refunctmedia.php)
- 2011 : Chris Burden. [Metropolis](http://www.artlog.com/2012/127-chris-burdens-metropolis#.UnSAwUPib0o)
- 2011 : Adam Harvey. [CV Dazzle](http://cvdazzle.com/)
- 2015 : Cécile Babiole et Jean-Marie Boyer. [Conversation au fil de l'eau](https://babiole.net/conversation-fil-de-leau/)
- 2016 : Recoded Exhibition, School For Poetic Computation
- 2017 : MaxLackner. Reset Social Media‏
- 2017 : Antoine Schmitt, Damien Gernay et Reso-nance Numérique. [Light Orchestra Device](http://www.antoineschmitt.com/lod-fr/)


## Autres ressources
- [Très humain plutôt que transhumain](https://www.youtube.com/watch?v=cR0T5-a6YTc), conférence d'Alain Damasio, écrivain de Science-Fiction
- [Khan academy - Théorie de l'information](https://www.khanacademy.org/computing/computer-science/informationtheory/info-theory/v/intro-information-theory)
- Livre [Le Bug humain](https://fr.wikipedia.org/wiki/Le_Bug_humain) de Sébastien Bohler
- émission France Inter Darknet
- documentaire Cambridge Analytica, Snowden, Manning, ... quadrature du net
- [Christopher Wylie : le lanceur d'alerte à l'origine du scandale Cambridge Analytica](https://www.franceinter.fr/emissions/l-instant-m/l-instant-m-11-mars-2020)
- [Extase du selfie](https://www.franceinter.fr/emissions/la-bande-originale/la-bande-originale-11-septembre-2019) de Philippe Delerm
- Questions sur la 5G, antennes, sécurité, lobby, monopoles
- Atelier partagé, mouvement DIY, ...
- Éric Sadin [La Vie Algorithmique](https://journals.openedition.org/lectures/17973)
- [Fermes à clics](https://fr.wikipedia.org/wiki/Ferme_%C3%A0_clics)
- Composition d'un téléphone, Documentaire : la salle guerre des terres rares
- Vidéos de [Datagueule](https://peertube.datagueule.tv/)
- [Environnement : les coûts cachés du numérique](https://www.curieux.live/2019/02/15/les-couts-caches-du-numerique-pour-lenvironnement/)
- [Guinée : main basse sur les ressources minières](https://www.franceinter.fr/environnement/guinee-main-basse-sur-les-ressources-minieres)
- [Obsolescence programmée : Apple mis en accusation](https://www.francetvinfo.fr/internet/apple/obsolescence-programmee-apple-mis-en-accusation_3151715.html)
- [Attention et société](http://www.internetactu.net/2020/02/19/attention-et-societe)
- [Documentaire les Invisibles](https://www.france.tv/slash/invisibles/replay-videos/), [Travailleurs du clic](https://www.franceculture.fr/emissions/la-vie-numerique/les-travailleurs-du-clic-ces-humains-caches-dans-les-machines).
- [Techlash : et si le retour de bâton de la « tech » était l’occasion de formuler un projet de société en matière technologique ?](http://maisouvaleweb.fr/techlash-retour-de-baton-de-tech-etait-loccasion-de-formuler-projet-de-societe-matiere-technologique/)
- [« onestla.tech », la parole aux travailleur.se.s](https://medias-cite.coop/onestlatech-la-parole-aux-travailleur-se-s/)
- [Quel Numérique pour demain ? #RESET](https://numeriquesolidaire.fr/wp-content/uploads/2020/02/cahier-enjeux_questions-numeriques_Reset_quel-numerique-voulons-nous_FING.pdf)
- [GPS : jusqu’où doit-on faire confiance à la machine ?](https://www.franceinter.fr/emissions/la-fenetre-de-la-porte/la-fenetre-de-la-porte-10-mars-2020), [Google Maps Hacks by Simon Weckert](https://youtu.be/k5eL_al_m7Q)
- [Internaute, libère-toi](https://actualite.nouvelle-aquitaine.science/internaute-libere-toi/)
- [Petites géolocalisations entre proches](https://korii.slate.fr/et-caetera/geolocalisation-entre-amis-famille-couple-zenly-life360-flicage-rassurer)
- [Paris : les goélands attaquent les drones de la police](http://www.leparisien.fr/faits-divers/les-goelands-attaquent-les-drones-de-la-prefecture-de-police-de-paris-25-06-2019-8102361.php)
- http://www.bertrandplanes.com/sans-titre/
- [Nicolas Nova : smartphones et wild-tech, « entre hautes-technologies et réappropriation par la rue »](https://www.makery.info/2020/07/13/nicolas-nova-les-smartphones-et-wild-tech-entre-hautes-technologies-et-reappropriation-par-la-rue/)
- [Peut-on limiter l’extension de la « société de la notation » ?](http://www.internetactu.net/2020/09/23/peut-on-limiter-lextension-de-la-societe-de-la-notation/)



## Travailleurs du clic
Les "travailleurs du clic", ces humains cachés dans les machines. Les “clickworkers” ou travailleurs du clic sont des gens qui travaillent chez eux, derrière leur ordinateur, à des horaires qui sont dictés par les clients, pour des tâches simples et répétitives, sans aucun statut et pour une rémunération minuscule. Leur travail, ils le trouvent sur des plateformes qui ont été créées par les géants de l’Internet, dont la plus connue est le Turc mécanique d’Amazon, ce sont des place de Grève contemporaines. On estime leur nombre à 500 000, ils sont principalement américains (à 75%, avec une grande part de femmes) ou des hommes indiens (aux alentours de 20%).

Coursiers à vélo, en scooter ou en voiture pour une plateforme de livraison de repas à domicile. Par beau temps, par pluie ou par neige...De l’autre côté de nos smartphones, ces travailleurs du clic attendent une commande qui ne viendra peut-être jamais. En attendant, ils roulent à faire progresser l’algorithme de la firme « partenaire » sans être rémunérés.

Les algorithmes répondent à nos envies, à nos désirs. Les moteurs de recherche nous comprennent mieux que quiconque et d’un seul clic, nous donnent satisfaction. Pourtant, aucune machine n’est encore capable d’apprendre seule et de raisonner comme un être humain. Et si de vraies personnes étaient employées à jouer les robots en attendant que ceux-ci existent réellement ?

## Dérives psycho-géographiques

Entre les divers procédés situationnistes, la dérive se définit comme une technique du passage hâtif à travers des ambiances variées. Le concept de dérive est indissolublement lié à la reconnaissance d'effets de nature psychogéographique, et à l'affirmation d'un comportemennt ludique-constructif, ce qui l'oppose en tous points aux notions classiques de voyage et de promenade.

Une ou plusieurs personnes se livrant à la dérive renoncent, pour une durée plus ou moins longue, aux raisons de se déplacer et d'agir qu'elles se connaissent généralement, aux relations, aux travaux et aux loisirs qui leur sont propres, pour se laisser aller aux sollicitations du terrain et des rencontres qui y correspondent.La part de l'aléatoire est ici moins déterminante qu'on ne croit : du point de vue de la dérive, il existe un relief psychogéographique des villes, avec des courants constants, des points fixes, et des tourbillons qui rendent l'accès ou la sortie de certaines zones fort malaisés.
