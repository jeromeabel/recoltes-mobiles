/*
 * Crash Message System : Letters Bot
 * 
 * Bluetooth HC06 > Arduino UNO > L298 Shield 2x2A DC (PWM Mode) + I2C 8x8 LED Matrix HT16K33
 *
 * Téléphone :
 * - Installer App : Blinky, Arduino Bluetooth Controller
 * - Activer Bluetooth
 * - Appairer : mot de passe 1234
 * - Quand la lumière du HC06 clignote = non appairé
 *
 * Utilisation :
 * - touche 'A' : droite 
 * - touche 'B' : gauche
 * - autre touche : arrêt
 * 
 * Dépendance : Adafruit_LEDBackpack
 *
 * Mapping 
 * 4   : L298 M1 DIRECTION	
 * 5   : L298 M1 PWM
 * 10  : Arduino RX > BT HC06 TX
 * 11  : Arduino TX > BT HC06 RX
 * A4  : MATRIX SDA
 * A5  : MATRIX SCL
 */

#include <SoftwareSerial.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

#define DEBUG true

// Mapping
#define M1_DIR 4 
#define M1_PWM 5
#define BT_RX 10 
#define BT_TX 11

// Variables
SoftwareSerial BT_serial(BT_RX, BT_TX);
Adafruit_8x8matrix matrix = Adafruit_8x8matrix();
int BT_data;

void setup()
{
  // Bluetooth
	pinMode(BT_RX, INPUT);
  pinMode(BT_TX, OUTPUT);
  BT_serial.begin(9600);

	// L298
  pinMode(M1_DIR, OUTPUT);

	// Matrix
  matrix.begin(0x70);  // address
	matrix.setTextSize(1);
  matrix.setTextWrap(false);
  matrix.setTextColor(LED_ON);
  matrix.clear();
  matrix.setCursor(1,1);
  matrix.print('%');
  matrix.writeDisplay();
  delay(2000);

	if (DEBUG) {
	  Serial.begin(9600);
    Serial.println("Letters Bot debugging");
	}

	delay(2000);
}

void loop()
{
	while(BT_serial.available()) {
		BT_data = BT_serial.read();

		if (DEBUG) {
			Serial.print("DEBUG : ");
			Serial.println(BT_data);
		}

		if( BT_data == 'A' ) {
			letterRandom();
			motorRight();
  		} else if( BT_data == 'B' ) {
			letterRandom();
			motorLeft();
  		} else {
			letterStop();
			motorStop();
  		}
	} // end while
}

void motorRight() {
	motorStop();
	digitalWrite(M1_DIR, HIGH);
 	for(int motor_value = 0 ; motor_value <= 255; motor_value += 5) {
   	analogWrite(M1_PWM, motor_value);
		delay(100);
  }
}

void motorLeft() {
	motorStop();
  digitalWrite(M1_DIR, LOW);
 	for(int motor_value = 0 ; motor_value <= 255; motor_value += 5) {
  	analogWrite(M1_PWM, motor_value);
		delay(100);
  }
}

void motorStop() {
  for(int motor_value = 255 ; motor_value >= 0; motor_value -= 5) {
   	analogWrite(M1_PWM, motor_value);
   	delay(20);
  }
}

void letterStop() {
  matrix.clear();
  matrix.writeDisplay();
}

// Une lettre majuscule au hasard
void letterRandom() {
  matrix.clear();
  matrix.setCursor(1,1);
  char letter = (int)(random(65, 90)); 
  matrix.print(letter);
  matrix.writeDisplay();
}
