// Crash Message System
// Sur une matrice de Leds 8x8 : une lettre au hasard et du texte

#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

void setup() {
  matrix.begin(0x70);  // address

  matrix.setTextSize(1);
  matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
  matrix.setTextColor(LED_ON);
  
}

void loop() {


  // Words scrolling
  for (int8_t x=0; x>=-150; x--) { // Changer
    matrix.clear();
    matrix.setCursor(x,0);
    matrix.print("JAMAIS DESESPERER !");
    matrix.writeDisplay();
    delay(100);
  }


/*
 // Random Letter
  matrix.clear();
  matrix.setCursor(1,1);
  //char letter = (int)(random(65, 90)); // Une lettre majuscule au hasard
  //matrix.print(letter);
  matrix.writeDisplay();
  delay(2000);
  */
}
