# SESSIONS
Avant chaque séance d'atelier, un exercice simple pourrait être mis en place par l'équipe pédagogique. Préparer le matériel en amont et déterminer les espaces disponibles pour travailler, laisser des choses, ainsi que le lieu de la restitution finale.

!météo : intérieur/extérieur


##  CFAA de Valdoie
Calendrier prévisionnel de la résidence :
- Mardi 29 septembre : 1ere journee de residence avec un temps de presentation de Jerome puis la realisation de l’installation ≪ Ferme a clics ≫ (avec avant un temps de presentation de ressources faisant echo a l’installation)
- Mardi 6 octobre : 2e journee de residence avec la realisation de l’installation ≪ Fantomes geographiques ≫(sorte de prothese de telephone pour la navigation ou l’envoi de messages)
- Mardi 1er décembre : 3e journee de residence avec la realisation de la 3e installation autour de l’envoi de messages sur des telephones se deplacant d’un point A a un point B le long de cordes tendues et/ou la preparation de la restitution ≪ Crash-test de telephones ≫.
- Mercredi 2 décembre : restitution de la residence avec le crash test des telephones et autres installations visibles et "activees"



## ONLINE : EXERCICE #1
- Thématiques : matières & numérique, sous-métier du numérique, dictat de l'évaluation
- Lire/voir/écouter une des ressources (voir le fichier ressources.md)
- Noter 3 mots positifs et négatifs pour espace public et téléphones portables.


Ressources :
- [Fermes à clics](https://fr.wikipedia.org/wiki/Ferme_%C3%A0_clics)
- [Fermes à clics fermée en Thaïlande](https://www.brut.media/fr/science-and-technology/une-ferme-a-clics-fermee-par-la-police-en-thailande-997acaed-1f1d-4059-bc0e-16c806e8d6a2)
- [Documentaire les Invisibles](https://www.france.tv/slash/invisibles/replay-videos/), [Travailleurs du clic](https://www.franceculture.fr/emissions/la-vie-numerique/les-travailleurs-du-clic-ces-humains-caches-dans-les-machines).
- [Peut-on limiter l’extension de la « société de la notation » ?](http://www.internetactu.net/2020/09/23/peut-on-limiter-lextension-de-la-societe-de-la-notation/)
- http://www.bertrandplanes.com/sans-titre/
- 2017 : https://www.max-lackner.com/stopthealgorithm
- 2009 : Robert Marthy. [Light Frequency Fingertips](http://www.robertmathy.com/lightfrequencyfingertips.html)
- [Black Mirror : l’application pour noter chaque être humain existe déjà](https://www.numerama.com/pop-culture/203998-black-mirror-nosedive-lapplication-pour-noter-chaque-etre-humain-existe-deja.html)

## SESSION #1



### Introduction
- 10min : présentation de la résidence/ateliers, des différents projets
- 20min : présentation de mon travail
- 30min : panorama des arts électroniques/médiatiques/numériques

pause

- 30min : échanges sur l'exercice
- 30min : présentation de certaines ressources : discussions
- 15min : présentation du projet du jour : réaliser une ferme à clic
- 15min : découpage du projet, des groupes

pause


### Projet : "clicocraty"
Un pupitre en bois avec 3 ou 4 rangées de téléphones portables en plâtre

<img src="dessins/clicocraty.jpg" width="700px">

- 15min : préparation de la salle
- 30min : tirages en plâtre de moules silicone déja fait avec encre conductrice. Attention au temps de séchage
- 15min : préparer le programme (l'interaction) : rélexions sur le rendu du programme. L'idée est que l'on appuie sur le téléphone est qu'on vote pour quelque chose ou pas (pouce en haut, pouce en bas)
- 15min : moulage silicone d'un téléphone
- 30min : pupitre en bois ou assemblage de profilés aluminium pour poser 12 ou 16 téléphones en plâtre, à poser sur une table par exemple
- connection des plâtres à l'interface électronique
- programmation des interactions et rendus visuels : initiation à la programmation ? 
- installation : besoin d'un écran et tests
- documentation

## ONLINE : EXERCICE #2
- Thématiques : prothèses numériques, données de navigations, psycho-géographie (random walk), ressources minières
- Lire/voir/écouter une des ressources
- Tirages de téléphones en plâtres


## SESSION #2
Finir le premier projet, commencer le second en pensant à la restitution.

### Introduction
- 30min : échanges sur l'exercice
- 30min : présentation de certaines ressources : discussions
- 15min : présentation du projet du jour
- 15min : découpage du projet, des groupes
- ? tirages en plâtre en plus

### Projet : "Fantômes géographiques"
Naviguer avec un téléphone et fixer sa position avec des ensembles de cordes au regard de données comme la position des antennes relais, les positions des ressources minières nécessaires pour fabriquer le téléphone, les trajets personnels, les destinataires des messages, ...

<img src="dessins/fantomes-geographiques.jpg" width="700px">

- préparer les kits de téléphones
- préparer la navigation : règles, objectifs (emplacements des ressources minières du téléphones, des antennes relais, des trajets dans une journée, ...)
- programmer la navigation : initiation à la programmation ou électronique ?
- préparation à la manipulation des cordes
- un groupe : 4 ou 5 personnes : 1 qui navigue avec le téléphone et 3 ou 4 autres qui nouent les cordes
- photos
- documentation

## ONLINE : EXERCICE #3
- Thématiques : écriture numérique, informations (codage, décodage), mémoire, éphémère ...
- Lire/voir/écouter une des ressources
- Tirage de téléphones en plâtres

## SESSION #3
Finir le deuxième projet et préparer la restitution.

### Introduction
- 20min : tirage plâtre : explications, préparation, premier tirage, nettoyage
- 1h: ressources, discussions
- 30min : échanges sur le projet du jour
- 15min : découpage du projet, des groupes

### Projet : "Crash Message System (CMS)"
Réaliser un système de messagerie par cordes et réaliser un système de crash tests

<img src="dessins/cms.jpg" width="700px">

- Tendre des fils dehors ou dedans de plusieurs mètres
- Système de messagerie via des téléphones qui circulent sur des cordes. Messages par écrits, crayons, craies, feutres, lumineux (système de morse ou bien message visible ensuite par la captation filmé)
- documentation


## RESTITUTION

**Performance finale**
- Tirer plusieurs fils
- préparer les téléphones en plâtre (éventuellement écrire des messages)
- lancement : les téléphones se cassent
- le tout filmé par une caméra haute vitesse
