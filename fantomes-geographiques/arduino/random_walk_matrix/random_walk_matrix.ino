// Fantômes géographiques
// Random Walk : 8 directions au hasard affichées sur une matrice 8x8 leds

#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

const int STEPS = 20;
int step_choice = 0;
int step_delay = 2000;
int step_count = 0;
boolean WAIT = true;

Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

// 8 directions
int rectCoord[8][2] = { 
  {0,0},
  {3,0},
  {6,0},
  {6,3},
  {6,6},
  {3,6},
  {0,6},
  {0,3},
  };

void setup() {
  Serial.begin(9600);  
  matrix.begin(0x70);  // pass in the address 
}
    
void loop() {

  // Beginning : 5 blinks
  if (WAIT) {
      for (int i = 0; i < 10; i++) {
        if (i % 2 != 0) {
          matrix.clear();
          matrix.writeDisplay();
        } else {
          matrix.clear();
          matrix.fillRect(3, 3, 2, 2, LED_ON);
          matrix.writeDisplay();
        }
        delay(1000);
     }
     WAIT = false;
     step_count = 0;
  } else {
	// Random directions
    step_choice = (int) random(8);
    step_delay = (int) random(2000, 6000);
    matrix.clear();
    matrix.fillRect(rectCoord[step_choice][0],rectCoord[step_choice][1], 2,2, LED_ON);
    matrix.writeDisplay();
    delay(step_delay);
    step_count++; 

	// End
    if (step_count > STEPS) {
      WAIT = true;
      matrix.clear();
      matrix.writeDisplay();
      delay(10000);
    }
  }
 
}
