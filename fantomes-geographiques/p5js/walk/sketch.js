let step_count = 0;
//let steps = [][];

let stepX, stepY, lastStepX, lastStepY;

function setup() {
  	createCanvas(windowWidth, windowHeight);
  	frameRate(20);
  	textSize(100);
 	background(0);
	stroke(255);
	strokeWeight(3);
	lastStepX = windowWidth / 2;
	lastStepY = windowHeight / 2;
}

function draw() {

	noStroke();
	fill(0,0,0,2);
	rect(0,0, windowWidth, windowHeight);

	if (frameCount % 20 == 0) {
	  	let randX = floor(random(-100,100));
	 	let randY = floor(random(-100,100));
		
		stepX = lastStepX + randX;
		stepY = lastStepY + randY;

		if (stepX < 0 && stepX > windowWidth) {
			stepX = lastStepX - randX;
		}

		if (stepY < 0 && stepY > windowHeight) {
			stepY = lastStepY - randY;
		}

		
		//stroke(random(255), random(255), random(255));
		stroke(255);
		strokeWeight(random(10,20));

		line(lastStepX, lastStepY, stepX, stepY);
		lastStepX = stepX;
		lastStepY = stepY;	
	}

}
