# Récoltes mobiles
Résidence artistique "Artistes plasticiens au lycée 2019/2020" de la région Bourgogne-Franche-Comté, avec l'association Juste ici et l'artiste Guillaume Bertrand.

## Présentation de mon travail
Je suis artiste et formateur multimédia depuis une quinzaine d'années. Le spectre de mes réalisations reflète mon envie de croiser les disciplines et les matières. Je développe des logiciels, des installations robotiques et numériques, et des performances audio-visuelles. Le tout sous licence libre, car selon moi l'acte de création devrait participer au partage des connaissances.

Pour en savoir plus et visualiser mon travail sous forme d'images, sans doute plus évocateur, vous pouvez visiter le site http://jeromeabel.net.

## Thématiques initiales
Les thématiques initiales croisent les intérêts de l'association Juste ici pour la création dans l'espace public et les réflexions de Guillaume Bertrand autour du téléphone mobile : "cet objet, dont le rôle est de nous connecter au monde, à l'immédiateté et aux autres a en réalité l'effet exactement inverse. Dans l'espace public, le smartphone coupe les promeneurs de leurs sens, physiologiquement. Plus de vue, plus d'ouïe,  plus de sensations physiques, la ville est parcourue desilhouettes courbées qu'il faut esquiver. Des silhouettes anesthésiées, au sens propre du terme."

Dans le fichier [recherches.md](./recherches.md), vous trouverez les différentes idées de départ avec les thématiques que j'ai souhaité aborder.

## Établissements

### CFAA Valdoie
- 6-15 élèves de Bac pro 1ère année Aménagement paysager (~ 16 ans)
- Document de travail en ligne : [framapad](https://semestriel.framapad.org/p/9eue-recoltesmobiles_cfa?lang=fr)
- Salle de classe, écran, ordinateur

###  EREA Simone Veil
- 35 élèves de CAP (1ère et 2e année) vente, peinture/revêtement, métallerie/ferronnerie, maçonnerie (15 - 18 ans). (Focus sur les 18 élèves vente et peinture/revêtement.)
- Potentiellement deux groupes dans deux salles distinctes sur les projets #2 et #3.
- Possibilité de scier du bois
- Document de travail en ligne : [framapad](https://semestriel.framapad.org/p/9eue-recoltesmobiles_erea?lang=fr)


## Planning

| Semaine  	| Lundi	|  EREA  (lundi) |  CFAA (mardi)	| Jérôme 	|  Commentaires 	|
| -------------  | ----- | -------------	| -------------- | -------------- |----------------------	|
| 36		| 31/08	| 		| (o)		|		|			|
| 37		| 07/09	| 		| (o)		|		|			|
| 38		| 14/09	| 		| 		|		|			|
| 39		| 21/09	| 		| 		| 		|			|
| 40		| 28/09	| o~		| o		| o o		|			|
| 41		| 05/10	| o~		| o		| o o		|			|
| 42		| 12/10	| o~		| 		| o x 		| restitution EREA	|
| 43		| 19/10	| 		| 		| 		| vacances		|
| 44		| 26/10	| 		| 		| 		| vacances		|
| 45		| 02/11	| 		| (o)		|		|			|
| 46		| 09/11	| 		| (o)		|		|			|
| 47		| 16/11	| 		| 		|		|			|
| 48		| 23/11	| (o~)		| (o)		| 		|			|
| 49		| 30/11	| (o~)		| o		| x o		| restitution CFAA	|
| 50		| 07/12	| 		| 		| 		|			|
| 51		| 14/12	| 		| 		| 		|			|


## Sessions
Découpage et déroulé des séances dans le fichier [sessions.md](./sessions.md).

## Matériel

### École
- Vidéo-projecteur
- Haut-parleur
- Écran HDMI, clavier, souris
- ordinateur
- Caméra haute vitesse
- Ordinateur
- Échelles

### Juste ici

Bois pour fabriquer les pupitres :
- 3 * tasseau médium (MDF) raboté, 22 x 22 mm ,L.2.44 m [exemple](https://www.leroymerlin.fr/v3/p/produits/tasseau-medium-mdf-rabote-22-x-22-mm-l-2-44-m-e156976)
- 1 * Lot de 6 tasseaux sapin petits nœuds bruts, 30 x 30 mm, L.2m [exemple](https://www.leroymerlin.fr/v3/p/produits/lot-de-6-tasseaux-sapin-petits-noeuds-bruts-30-x-30-mm-l-2m-e1502219762)
- 2 * panneau de médium (mdf) naturel, Ep.15 mm L.80 x l.60 cm [exemple](https://www.leroymerlin.fr/v3/p/produits/predecoupe-medium-mdf-naturel-ep-15-mm-l-80-x-l-60-cm-e105242)

Outils :
- visseuse/perçeuses + mèches et forêts
- Quelques bols pour mélanger le plâtre
- clés allen
- clés
- tournevis
- marteau
- maillet
- limes
- scie égoine
- ciseaux, cutter
- fers à souder, soudure
- 2 serre-joints
- petit rouleau de peinture pour les pupitres 
- pinces

Consommables :
- Chiffons
- colle à bois
- clous
- vis bois assez fines 3mm ou 3.5mm
- scotch
- pâte à bois
- papier pour ponçer
- bâche
- peinture bois blanche couvrante pour médium mdf


### Jérôme
- profilés aluminium pour fabriquer le pupitre et les supports de téléphone
- boulons, écrous
- moules silicone de plusieurs téléphone
- silicone
- plâtre pour 32 tirage
- raspberry + touchboard
- encre conductrice
- fils
- cordes

